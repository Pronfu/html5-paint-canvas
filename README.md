# html5-paint-canvas
Want to paint in your browser? Using HTML5? You have come to the right place.

![Example of what can be done](https://i.imgur.com/wH7yRma.png) 

## License
[Unlicense](https://unlicense.org/)

## Live Demo
[https://projects.gregoryhammond.ca/html5-paint-canvas/](https://projects.gregoryhammond.ca/html5-paint-canvas/)